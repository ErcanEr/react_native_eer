import React from "react";
import { useState } from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";

const CounterScreen = () => {
  const [counter, setCounter] = useState(() => 0);

  return (
    <View style={styles.viewStyle}>
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => setCounter((prevState) => prevState + 1)}
      >
        <Text style={styles.textStyle}>Increase</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => setCounter((prevState) => prevState - 1)}
      >
        <Text style={styles.textStyle}>Decrease</Text>
      </TouchableOpacity>
      <Text style={styles.counterTextStyle}>Current Counter</Text>
      <Text style={styles.counterTextStyle}>{counter}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    alignItems: "center",
  },
  buttonStyle: {
    backgroundColor: "#5533ff",
    width: 150,
    marginTop: 10,
    borderRadius: 5,
    alignItems: "center",
  },
  textStyle: {
    color: "#fff",
    fontSize: 24,
  },
  counterTextStyle: {
    color: "#000",
    fontSize: 32,
  },
});

export default CounterScreen;
