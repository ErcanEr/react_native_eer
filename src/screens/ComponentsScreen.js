import React from "react";
import { Text, StyleSheet, View } from "react-native";

const CompponentsScreen = () => {
  const yourname = "Ercan Er";

  return (
    <View>
      <Text style={styles.textStyle}>Getting started with React Native!</Text>
      <Text style={styles.subHeaderTitle}>My name is {yourname} </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 45,
    color: "#000",
  },
  subHeaderTitle: {
    fontSize: 20,
    color: "#000",
  },
});

export default CompponentsScreen;
