import React from "react";
import { Text, StyleSheet, Button, View, TouchableOpacity } from "react-native";

const HomeScreen = ({ navigation }) => {
  return (
    <View>
      <Text style={styles.textHeader}>TouchableOpacity onPress Buttons</Text>
      <TouchableOpacity
        style={styles.touchableStyle}
        onPress={() => navigation.navigate("Components")}
      >
        <Text style={styles.text}>Go to component demo</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.touchableStyle}
        onPress={() => navigation.navigate("List")}
      >
        <Text style={styles.text}>Go to list demo</Text>
      </TouchableOpacity>
      <Text style={styles.textHeader}>Simple Button onPress</Text>
      <Button
        title="Go to other home menu"
        onPress={() => navigation.navigate("Home")}
      />
      <Button
        title="Go to component demo"
        onPress={() => navigation.navigate("Components")}
      />
      <Button
        title="Go to list demo"
        onPress={() => navigation.navigate("List")}
      />
      <Button
        title="Go to Image list demo"
        onPress={() => navigation.navigate("ImageScreen")}
      />
      <Button
        title="Go to Counter Screen"
        onPress={() => navigation.navigate("Counter")}
      />
      <Button
        title="Go to Color Screen"
        onPress={() => navigation.navigate("Color")}
      />
      <Button
        title="Go to Square Screen"
        onPress={() => navigation.navigate("Square")}
      />
      <Button
        title="Go to Box Screen"
        onPress={() => navigation.navigate("Box")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    textAlign: "center",
    color: "#fff",
    backgroundColor: "#32965D",
    borderWidth: 1,
    padding: 10,
  },
  touchableStyle: {
    marginTop: 10,
  },
  textHeader: {
    fontSize: 24,
    textAlign: "center",
    fontWeight: "bold",
    marginTop: 10,
    marginBottom: 10,
  },
});

export default HomeScreen;
