import React from "react";
import { View } from "react-native";

const ColorSquare = ({ redValue = 255, greenValue = 0, blueValue = 0 }) => {
  return (
    <View style={{ alignItems: "center" }}>
      <View
        style={{
          backgroundColor: `rgb(${redValue}, ${greenValue},${blueValue})`,
          width: 200,
          height: 200,
          marginTop: 20,
        }}
      ></View>
    </View>
  );
};

export default ColorSquare;
