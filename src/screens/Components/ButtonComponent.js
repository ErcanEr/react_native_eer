import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";

const ButtonComponent = ({ onClick, title }) => {
  return (
    <View style={styles.viewStyle}>
      <TouchableOpacity onPress={onClick} style={styles.buttonStyle}>
        <Text style={styles.textStyle}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    alignItems: "center",
    marginTop: 10,
  },

  buttonStyle: {
    width: 200,
    alignItems: "center",
    backgroundColor: "#24a0ed",
    borderRadius: 5,
    padding: 15,
  },
  textStyle: {
    textAlign: "center",
    fontSize: 18,
    color: "#fff",
  },
});

export default ButtonComponent;
