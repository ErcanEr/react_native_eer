import React from "react";
import { Text, View, Image, StyleSheet } from "react-native";

const ImageList = ({ imgUrl, title, imageScore }) => {
  return (
    <View>
      <View style={styles.subViewStyle}>
        <Image
          style={{ width: 100, height: 100, borderRadius: 50 }}
          source={{ uri: imgUrl }}
        />
        <View style={styles.textContainer}>
          <Text style={styles.textStyle}>{title}</Text>
          <Text style={styles.textStyle}>Images Score - {imageScore}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  subViewStyle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    margin: 10,
  },
  textStyle: {
    fontSize: 16,
    fontWeight: "400",
    marginLeft: 5,
  },
  textContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
});

export default ImageList;
