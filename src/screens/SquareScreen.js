import React, { useReducer } from "react";
import { View, Text } from "react-native";
import ButtonComponent from "./Components/ButtonComponent";
import ColorSquare from "./Components/ColorSquare";

const CHANGE_COLOR = 15;

const reducer = (state, action) => {
  switch (action.colorToChange) {
    case "red":
      return state.red + action.amount > 255 || state.red + action.amount < 0
        ? state
        : { ...state, red: state.red + action.amount, comment: "Red process" };
    case "green":
      return state.green + action.amount > 255 ||
        state.green + action.amount < 0
        ? state
        : {
            ...state,
            green: state.green + action.amount,
            comment: "Green Process",
          };

    case "blue":
      return state.blue + action.amount > 255 || state.blue + action.amount < 0
        ? state
        : { ...state, blue: state.blue + action.amount };

    default:
      return state;
  }
};

const SquareScreen = () => {
  const [state, dispatch] = useReducer(reducer, { red: 0, green: 0, blue: 0 });
  const { red, green, blue, comment } = state;

  return (
    <View>
      <Text style={{ textAlign: "center" }}>Red</Text>
      <ButtonComponent
        title="More Red"
        onClick={() => dispatch({ colorToChange: "red", amount: CHANGE_COLOR })}
      />
      <ButtonComponent
        title="Less Red"
        onClick={() =>
          dispatch({ colorToChange: "red", amount: -1 * CHANGE_COLOR })
        }
      />
      <Text style={{ textAlign: "center" }}>Green</Text>
      <ButtonComponent
        title="More Green"
        onClick={() =>
          dispatch({ colorToChange: "green", amount: CHANGE_COLOR })
        }
      />
      <ButtonComponent
        title="Less Green"
        onClick={() =>
          dispatch({ colorToChange: "green", amount: -1 * CHANGE_COLOR })
        }
      />
      <Text style={{ textAlign: "center" }}>Blue</Text>
      <ButtonComponent
        title="More Blue"
        onClick={() =>
          dispatch({ colorToChange: "blue", amount: CHANGE_COLOR })
        }
      />
      <ButtonComponent
        title="Less Blue"
        onClick={() =>
          dispatch({ colorToChange: "blue", amount: -1 * CHANGE_COLOR })
        }
      />
      <ColorSquare redValue={red} greenValue={green} blueValue={blue} />
      <Text>{comment}</Text>
    </View>
  );
};

export default SquareScreen;
