import React from "react";
import { FlatList, View } from "react-native";
import ImageList from "./Components/ImageList";

const dataList = [
  {
    imageUrl: "https://picsum.photos/id/237/200/300",
    title: "Cute Dog",
    imageScore: 5,
  },
  {
    imageUrl: "https://picsum.photos/seed/picsum/200/300",
    title: "Random Image",
    imageScore: 9,
  },
  {
    imageUrl: "https://picsum.photos/200/300?grayscale",
    title: "Grey Scale",
    imageScore: 7,
  },
  {
    imageUrl: "https://picsum.photos/200/300/?blur",
    title: "Blur Photo",
    imageScore: 1,
  },
];

const ImageScreen = () => {
  return (
    <View>
      <FlatList
        data={dataList}
        keyExtractor={(item) => item.imageUrl}
        renderItem={({ item }) => {
          return (
            <ImageList
              imgUrl={item.imageUrl}
              title={item.title}
              imageScore={item.imageScore}
            />
          );
        }}
      />
    </View>
  );
};

export default ImageScreen;
