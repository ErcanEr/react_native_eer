import React from "react";
import { useState } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from "react-native";

const ColorScreen = () => {
  const [colors, setColors] = useState([]);

  const randomColor = () => {
    const obj = "#" + Math.random().toString(16).substr(-6);
    setColors((oldArray) => [...oldArray, obj]);
  };

  return (
    <View style={styles.viewStyle}>
      <TouchableOpacity onPress={randomColor} style={styles.buttonStyle}>
        <Text style={styles.buttonTextStyle}>Add Color</Text>
      </TouchableOpacity>
      <FlatList
        data={colors}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item}
        renderItem={({ item }) => {
          return (
            <View style={styles.viewContainerStyle}>
              <View
                style={{ backgroundColor: item, width: 200, height: 200 }}
              ></View>
              <Text style={styles.textStyle}>{item}</Text>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    alignItems: "center",
  },
  textStyle: {
    fontSize: 24,
    fontWeight: "500",
    marginLeft: 10,
  },
  buttonTextStyle: {
    fontSize: 24,
    fontWeight: "500",
    color: "#fff",
  },
  viewContainerStyle: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  buttonStyle: {
    backgroundColor: "#6752e9",
    alignItems: "center",
    width: 240,
    padding: 30,
    textAlign: "center",
    marginBottom: 20,
  },
});

export default ColorScreen;
