import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const LessonsScreen = ({ navigation }) => {
  return (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate("Counter")}>
        <Text style={styles.text}>Use Reducer Counter</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate("TextInput")}>
        <Text style={styles.text}>Text Input</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    color: "green",
  },
});

export default LessonsScreen;
