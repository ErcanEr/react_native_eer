import React, { useReducer } from "react";
import { Text, View, StyleSheet, TextInput } from "react-native";

const initialState = { password: "" };

function reducer(state, action) {
  switch (action.type) {
    case "passwordChange":
      return { ...state, password: action.newValue };
    default:
      return new Error();
  }
}

const TextInputScreen = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const { password } = state;

  return (
    <View>
      <Text>Password:</Text>
      <TextInput
        style={styles.input}
        autoCapitalize="none"
        autoCorrect={false}
        secureTextEntry={true}
        value={password}
        onChangeText={(newValue) =>
          dispatch({ type: "passwordChange", newValue })
        }
      />
      {password !== "" && password.length < 5 ? (
        <Text>Password Must be 5 Characters</Text>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    margin: 10,
    borderWidth: 1,
    borderColor: "#000",
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 2,
    paddingRight: 2,
  },
});

export default TextInputScreen;
