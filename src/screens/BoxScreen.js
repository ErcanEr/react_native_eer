import React from "react";
import { Text, View, StyleSheet } from "react-native";

const BoxScreen = () => {
  return (
    <View style={styles.viewStyle}>
      <Text style={styles.textStyle}>Box Screen</Text>
      <Text style={styles.textStyle}>Box Screen 2</Text>
      <Text style={styles.textStyle}>Box Screen 3</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    borderWidth: 5,
    borderColor: "black",
    alignItems: "center", // Default value stretch
    flexDirection: "row", // Default value column
  },
  textStyle: {
    borderWidth: 1,
    borderColor: "red",
    marginVertical: 10,
    marginHorizontal: 10,
  },
});

export default BoxScreen;
