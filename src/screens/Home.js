import React from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";

const Home = ({ navigation }) => {
  return (
    <View>
      <TouchableOpacity onPress={() => navigation.navigate("Photos")}>
        <Text style={styles.text}>Home Screen</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate("Lessons")}>
        <Text style={styles.text}>Lessons Screen</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    color: "purple",
  },
});
export default Home;
