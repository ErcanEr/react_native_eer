import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

const ListScreen = () => {
  const friends = [
    {
      name: "Friend 1",
      age: 20,
    },
    {
      name: "Friend 2",
      age: 45,
    },
    {
      name: "Friend 3",
      age: 32,
    },
    {
      name: "Friend 4",
      age: 27,
    },
    {
      name: "Friend 5",
      age: 53,
    },
    {
      name: "Friend 6",
      age: 77,
    },
    {
      name: "Friend 7",
      age: 91,
    },
  ];
  return (
    <View>
      <FlatList
        // horizontal propsunu verirsek itemler yatay olarak sıralanır ve yatay olarak slider hareket eder.
        // showsHorizontalScrollIndicator={false} yaparsak yatay slider ı gizleyecektir.
        keyExtractor={(friend) => friend.name + (Math.random() * 100).toFixed()}
        data={friends}
        renderItem={({ item }) => {
          return (
            <Text style={styles.textStyle}>
              {item.name} - Age {item.age}
            </Text>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    marginVertical: 10,
    borderWidth: 2,
    borderColor: "black",
    borderRadius: 2,
    padding: 10,
  },
});

export default ListScreen;
