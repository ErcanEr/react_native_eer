import React, { useReducer } from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";

const initialState = { counter: 0 };

function reducer(state, action) {
  switch (action.type) {
    case "change_increase":
      return { ...state, counter: state.counter + action.payload };
    case "change_decrease":
      return state.counter <= 0
        ? { ...state, counter: state.counter }
        : { ...state, counter: state.counter - action.payload };
    default:
      throw new Error();
  }
}

const CounterScreenReducer = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const { counter } = state;

  return (
    <View style={styles.viewStyle}>
      <TouchableOpacity
        style={styles.buttonText}
        onPress={() => dispatch({ type: "change_increase", payload: 1 })}
      >
        <Text>Increase + 1</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonText}
        onPress={() => dispatch({ type: "change_decrease", payload: 1 })}
      >
        <Text>Decrease - 1</Text>
      </TouchableOpacity>
      <Text>{counter}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonText: {
    color: "#fff675",
    backgroundColor: "#099",
    fontSize: 20,
    padding: 10,
    borderRadius: 5,
    margin: 10,
  },
  viewStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
});

export default CounterScreenReducer;
